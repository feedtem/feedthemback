require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Proj
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2
    config.action_controller.allow_forgery_protection = false

    config.middleware.use Rack::Cors do
      allow do
        origins '*'
        resource '*',
        headers: :any,
        expose: ['access-token', 'expiry', 'token-type', 'uid', 'client'],
        methods: [:get, :post, :options, :delete, :put]
      end
    end
    config.to_prepare do
      DeviseTokenAuth::SessionsController.skip_before_action :authenticate_user!, raise: false
      DeviseTokenAuth::PasswordsController.skip_before_action :authenticate_user!, raise: false
      DeviseTokenAuth::RegistrationsController.skip_before_action :authenticate_user!, raise: false
      DeviseTokenAuth::TokenValidationsController.skip_before_action :authenticate_user!, raise: false
      DeviseTokenAuth::SessionsController.skip_before_action :authenticate_admin!, raise: false
      DeviseTokenAuth::PasswordsController.skip_before_action :authenticate_admin!, raise: false
      DeviseTokenAuth::RegistrationsController.skip_before_action :authenticate_admin!, raise: false
      DeviseTokenAuth::TokenValidationsController.skip_before_action :authenticate_admin!, raise: false

    end  
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
  end
end

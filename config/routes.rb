Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  devise_for :admins

  mount_devise_token_auth_for 'User', at: 'auth'
  namespace :api do
    namespace :v1 do
      # mount_devise_token_auth_for 'User', at: 'auth'
      resources :donations
      resources :distributions
      resources :distribution_requests, only: :create
    end
  end

  
  # devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

class Donation < ApplicationRecord
  has_one_attached :avatar
  belongs_to :user
  has_many :distribution_requests
  validates :food_name, presence: true
  validates :quantity, presence: true, numericality: { only_integer: true, greater_than: 0 }
  enum status: { pending: 0, distribution_approved: 1, distributed: 2 }

  def status_for_user(user_id)
    request = distribution_requests.find_by(user_id: user_id)
    'Request Submitted' if request.present?
  end
  
end

class Distribution < ApplicationRecord
  belongs_to :distributor, class_name: 'User', foreign_key: 'distributor_id'
  belongs_to :donation
end

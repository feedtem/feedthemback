class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :validatable
  include DeviseTokenAuth::Concerns::User
  has_many :donations
  has_many :distribution_requests

  # validates :password_confirmation, presence: true  
end

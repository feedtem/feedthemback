class DistributionRequest < ApplicationRecord
  belongs_to :user
  belongs_to :donation
  validates :donation_id, uniqueness: { scope: :user_id, message: 'already requested.' }


  validate :donation_availability

  def donation_availability
    unless donation.pending?
      errors.add(:donation, "is already #{donation.status}.")
    end
  end

end

module Api
  module V1
    class BaseController < ApplicationController
      include DeviseTokenAuth::Concerns::SetUserByToken
      # before_action :authenticate_api_v1_user!
      before_action :authenticate_user!
      before_action :configure_permitted_parameters, if: :devise_controller?
      
      skip_before_action :authenticate_admin!

      # def current_user
      #   current_api_v1_user
      # end

      protected

      def configure_permitted_parameters
        added_attrs = [:email, :password, :password_confirmation, :name]
        devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
      end
    end
  end
end

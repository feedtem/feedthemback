module Api
  module V1
    class DistributionRequestsController < BaseController
      
      def create
        distribution_request = DistributionRequest.new(distribution_request_params)
        distribution_request.user_id = current_user.id
        if distribution_request.save
          render json: {
            message: 'Your request has been saved. Thank you!',
            status: 'Request Submitted'
          }, status: 200
        else
          render json: {
            errors: distribution_request.errors.full_messages
          }, status: :unprocessable_entity
        end 
      end

      private

      def distribution_request_params
        params.require(:distribution_request)
              .permit(:reason, :donation_id)
      end

    end
  end
end
module Api
  module V1
    class DistributionsController < BaseController
      def index
        distributions =
          Distribution.includes(:distributor, :donation)
                      .find_each
                      .map do |distribution|
                        {
                          id: distribution.id,
                          donated_item: distribution.donation.food_name
                        }
                      end
        render json: {
          distributions: distributions
        }, status: 200
      end

      def update
        # distribution = Distribution.find(params[:id])
        # distribution.assign_attributes(distribution_params)
        # distribution.status = 'distributed'
        # if distribution.save
        #   render json: { message: 'Updated Successfully.' }, status: 200
        # else
        #   render json: { message: distribution.errors.messages }, status: :unprocessable_entity
        # end
        render json: { message: 'Updated Successfully.' }, status: 200
      end

      private

      def distribution_params
        params.require(:distribution)
              .permit(:received_by, :received_date)
      end
      
    end
  end
end

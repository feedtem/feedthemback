module Api
  module V1    
    class DonationsController < BaseController
      before_action :set_donation, only: [:show, :edit, :update, :destroy, :fetch_detail]
      def index
       donations = Donation.includes(:distribution_requests)
                           .map do |donation|
                            {
                              id: donation.id,
                              food_name: donation.food_name,
                              status: donation.status_for_user(current_user.id) || donation.status,
                              quantity: donation.quantity,
                              donater_name: donation.donater_name,
                              donater_contact: donation.donater_contact,
                              remark: donation.remark
                            }
                           end
       render json: {
        donations: donations
       }, status: 200

      end
      def show
        @donation = Donation.find(parmas[:id])
      end

      def create
        @donation = Donation.new(donation_params)
        @donation.user_id = current_user.id
        if @donation.save
          render json: { success: true, message: 'Successfully Donated.' }, status: 200
        else
          render json: { errors: @donation.errors }, status: :unprocessable_entity
        end
      end

      def update
        respond_to do |format|
          if @donation.save
            format.html { redirect_to @donation, notice: 'Change on Donation was made Sucessfully.' }
            format.json { render :show, status: :created, location: @donation }
          else
            format.html { render :update }
            format.json { render json: @donation.errors, status: :unprocessable_entity }
          end
        end
      end
      def fetch_detail
        render json: {
          food_name: @donation.title,
          quantity: @donation.quantity,
          latitude: @donation.latitude,
          longitude: @donation.longitude,
          donater_name: @donation.donaterName,
          donater_contact: @donation.donater_contact,
          remark: @donation.remark,
          avatar: @donation.avatar,
          status: @donations.status,
        }
      end

      def destroy
        @donation.destroy
        respond_to do |format|
          format.html { redirect_to donations_url, notice: 'donation was successfully destroyed.' }
          format.json { head :no_content }
        end
      end

      private
      
      def set_donation
        @donation = Donation.find(params[:id])  
      end

      def donation_params
        params.require(:donation)
              .permit(:food_name, :quantity ,:latitude,
                      :longitude, :donater_name, :donater_contact,
                      :remark, :avatar, :status
                     )
      end
    end
  end
end

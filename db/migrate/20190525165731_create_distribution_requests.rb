class CreateDistributionRequests < ActiveRecord::Migration[5.2]
  def change
    create_table :distribution_requests do |t|
      t.references :user
      t.references :donation
      t.text :reason

      t.timestamps
    end
  end
end

class CreateDonations < ActiveRecord::Migration[5.2]
  def change
    create_table :donations do |t|
      t.string :food_name
      t.integer :quantity
      t.float :latitude
      t.float :longitude
      t.string :donater_name
      t.integer :donater_contact
      t.string :remark
      t.string :avatara
      t.timestamps
    end
  end
end

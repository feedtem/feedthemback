class ChangeDataTypeofPhone < ActiveRecord::Migration[5.2]
  def up
    change_column :donations, :donater_contact, :string
  end
  
  def down
    change_column :donations, :donater_contact, :integer
  end
end

class CreateDistributions < ActiveRecord::Migration[5.2]
  def change
    create_table :distributions do |t|
      t.bigint :distributor_id, index: true
      t.references :donation
      t.string :received_by
      t.boolean :distributed
      t.date :received_date
      t.string :received_contact

      t.timestamps
    end
  end
end
